
const gStudent = ["name", "birthday", "email", "phone", "action"];
const gNAME_COL = 0;
const gBD_COL = 1;
const gEMAIL_COL = 2;
const gPHONE_COL = 3;
var gACTION_COL = 4;
var gId = [];
$(document).ready(function () {
    onPageLoading()
    $(document).on('click', '#table-id .update-btn', function () {
        onBtnUpdateStudent(this);
    });
    $(document).on('click', '#table-id .delete-btn', function () {
        onBtnDeleteStudent(this);
    })
});

function onPageLoading() {
    callApiServerRequest();
    $("#table-id").DataTable({
        columns: [
            { data: gStudent[gNAME_COL] },
            { data: gStudent[gBD_COL] },
            { data: gStudent[gEMAIL_COL] },
            { data: gStudent[gPHONE_COL] },
            { data: gStudent[gACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                className: "w-25",
                defaultContent:
                    `<div class='row'><div class='col-6 pr-0'><button style='border: none;' class='update-btn text-info'><i class='fas fa-edit'></i> Chỉnh sửa</button></div> 
                    ` + "|" + `
                    <button style='border: none;' class='text-danger delete-btn'><i class='fa fa-trash-alt'></i> Xóa</button></div></div>`
            },
            {
                "defaultContent": "-",
                "targets": "_all"
            }
        ]
    });

}

function callApiServerRequest() {
    $.ajax({
        url: "https://devcamp-student.herokuapp.com/users/",
        type: "GET",
        async: true,
        dataType: "json",
        success: function (responseJson) {
            console.log(responseJson)
            loadDataToTable(responseJson);
        }
    });
};

function loadDataToTable(paramResponseJson) {
    console.log(paramResponseJson);
    var vTbale = $("#table-id").DataTable();
    vTbale.clear();
    vTbale.rows.add(paramResponseJson);
    vTbale.draw();
}

function onBtnUpdateStudent(paramUpdate) {
    var vTable = $("#table-id").DataTable();
    var vCurrentRow = $(paramUpdate).closest("tr");
    var vRowData = vTable.row(vCurrentRow).data();
    console.log("ID student là: " + vRowData.id);
    gId = vRowData.id;
}
function onBtnDeleteStudent(paramDelete) {
    var vTable = $("#table-id").DataTable();
    var vCurrentRow = $(paramDelete).closest("tr");
    var vRowData = vTable.row(vCurrentRow).data();
    console.log("ID student là: " + vRowData.id);
    gId = vRowData.id;
}